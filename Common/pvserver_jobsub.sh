#!/bin/bash
# ParaView Server job submission wrapper script. 
# Maintainer: Chuck Atkins @ Kitware

BASE_DIR=$(dirname $(readlink -f ${BASH_SOURCE}))
. ${BASE_DIR}/pvserver_jobsub.functions


# Print usage
usage() {
  cat<<EOF
Usage: $0 NUM_NODES NUM_PPN HOURS PV_SERVER_PORT PV_VERSION_FULL CONF [VAR=VAL ...]
 
  NUM_NODES       - Number of compute nodes to use.
  NUM_PPN         - Number of processes per compute node.
  HOURS           - Job duration in hours.
  PV_SERVER_PORT  - Port number for pvserver to use
  PV_VERSION_FULL - ParaView version string
  CONF            - Name of configuration file to load
  VAR=VAL         - Any extra variables to export
EOF
}

# Cleanup on exit
cleanup() {
  echo "Cleaning up"
  if [ -n "${JOBID}" ]
  then
    cancel_job ${JOBID}
  fi
  cleanup_tunnel
}

# Error handling and cleanup
trap cleanup EXIT INT TERM SIGKILL

if [ $# -lt 6 ]
then
  usage
  exit 1
fi

export NUM_NODES=$1
export NUM_PPN=$2
export HOURS=$3
export PV_SERVER_PORT=$4
export PV_VERSION_FULL=$5
CONF=$6
shift 6

echo "Exporting additional variables"
export_vars "$@"

if [ -f ${CONF} ]
then
  CONF_PATH=${CONF}
elif [ -f ${BASE_DIR}/${CONF} ]
then
  CONF_PATH=${BASE_DIR}/${CONF}
else
  echo "Error: Config file ${CONF} does not exist"
  sleep 10
  exit 2
fi
echo "Loading config file ${CONF}"
. ${CONF_PATH}

echo "Setting up tunnel pre-submission"
if ! setup_tunnel_pre
then
  echo "Error setting up connection tunnel"
  sleep 10
  exit 3
fi

echo "Submitting job for ${NUM_NODES} and ${NUM_PPN} PPN"
if ! submit_job
then
  echo "Error submitting job"
  sleep 10
  exit 4
fi

echo "Waiting for job to run"
S=0
job_status ${JOBID}
JOB_STATUS=$?
while [ ${JOB_STATUS} -eq 1 ]
do
  sleep_print 5
  job_status ${JOBID}
  JOB_STATUS=$?
done
echo ""
if [ ${JOB_STATUS} -eq 2 ]
then
  echo "Error: job unable to start"
  sleep 10
  exit 5 
fi
echo "Setting up tunnel post-allocation"
if ! setup_tunnel_post
then
  echo "Error setting up connection tunnel"
  sleep 10
  exit 6
fi

echo ""
echo "ParaView allocation granted. Initating reverse connection between"
echo "pvserver and your ParaView client."
echo ""

echo "Waiting for job to exit"
S=0
job_status ${JOBID}
JOB_STATUS=$?
while [ ${JOB_STATUS} -eq 0 ]
do
  sleep_print 5
  job_status ${JOBID}
  JOB_STATUS=$?
done
echo ""
