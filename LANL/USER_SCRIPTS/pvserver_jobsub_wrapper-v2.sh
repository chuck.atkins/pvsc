#!/bin/bash
# ParaView Server job submission wrapper script.

usage() {
  cat<<EOF
    Usage: $0 NUM_NODES NUM_PPN HOURS PV_SERVER_PORT ACCOUNT QUEUE PV_VERSION_FULL
   
      NUM_NODES       - Number of compute nodes to use.
      NUM_PPN         - Number of processes per compute node.
      HOURS           - Job duration in hours.
      PV_SERVER_PORT  - Port number for pvserver to use
      ACCOUNT         - Account to use for the job
      QUEUE           - Queue to use for the job
      PV_VERSION_FULL - ParaView version string
EOF
}

# Cleanup on exit
cleanup() {
  echo "Cleaning up"
  if [ -n "${JOBID}" ]
  then
    echo "  Cancelling job if still running"
    canceljob ${JOBID}
  fi
  if [ -n "${SOCAT_PID}" ]
  then
    echo "  Killing socat tunnel if still remaining"
    kill ${SOCAT_PID} 1>/dev/null 2>/dev/null
  fi
}

# Error handling and cleanup
trap cleanup EXIT INT TERM SIGKILL

if [ $# -ne 7 ]
then
  usage
  exit 1
fi

NUM_NODES=$1
NUM_PPN=$2
HOURS=$3
PV_SERVER_PORT=$4
ACCOUNT=$5
QUEUE=$6
PV_VERSION_FULL=$7

BASE_DIR=$(dirname ${BASH_SOURCE})

# Ensure the User is able to see the DOE "Notice to Users" banner.
cat /etc/motd
sleep 5
cat ${BASE_DIR}/ParaView_figlet_iso1.txt

# Locate the appropriate config file
for CFG in ${BASE_DIR}/*.cfg
do
  if [ $(expr "$(hostname -s)" : "$(basename ${CFG} .cfg)") -gt 0 ]
  then
    break
  fi
done
if [ -z "${CFG}" ]
then
  echo "No config file for the current host is found"
  exit 1
fi
echo "Loading config $(basename ${CFG})"
source ${CFG}

if [ ${NUM_PPN} -gt ${MAX_PPN} ]
then
  echo "WARNING: Requested PPN ${NUM_PPN} is larger than the max available"
  echo "         Reducing to ${MAX_PPN}"
  NUM_PPN=${MAX_PPN}
fi

echo "Submitting job for ${NUM_NODES} nodes at ${NUM_PPN} procs per node"
FE_IP=$(/sbin/ip addr show dev ${FE_IF} | sed -n 's|.*inet \([0-9\.]*\).*|\1|p')
JOBID=$(job_submit)
echo "Job ID ${JOBID}"

echo -n "Waiting for job to enter a running state "
s=0
while ! job_running ${JOBID}
do
  sleep 10
  s=$((s+10))
  if [ $((s%3600)) -eq 0 ]
  then
    echo -n '|'
  elif [ $((s%60)) -eq 0 ]
  then
    echo -n '*'
  else
    echo -n "."
  fi
done
echo ""

echo "Job running"

echo "Starting socat tunnel ${FE_IP}:${PV_SERVER_PORT} -> localhost:${PV_SERVER_PORT}"
export PATH=$(dirname ${BASH_SOURCE}):${PATH}
socat -d -d -lf socat-fe.log \
  tcp4-listen:${PV_SERVER_PORT},bind=${FE_IP},close \
  tcp4-connect:localhost:${PV_SERVER_PORT} &
SOCAT_PID=$!

echo -n "Waiting for job to finish "
s=0
while job_running ${JOBID}
do
  sleep 10
  s=$((s+10))
  if [ $((s%3600)) -eq 0 ]
  then
    echo -n '|'
  elif [ $((s%60)) -eq 0 ]
  then
    echo -n '*'
  else
    echo -n "."
  fi
done
echo ""

trap -- EXIT INT TERM SIGKILL
cleanup
