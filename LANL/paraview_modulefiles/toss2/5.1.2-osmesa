#%Module
# vi:set filetype=tcl:

source /usr/projects/hpcsoft/utilities/lib/envmodules_header

# local variables
set name    [moduleName]
set version [moduleVersion]
set machine [machineName]
set os      [systemOS]



proc ModulesHelp { } {
    puts stderr "\tParaview v$version"
    puts stderr "\n\tThis module file will configure your environment appropriately for using Paraview $version."
}

module-whatis   "Paraview v$version"

conflict paraview

# Determine the major version of paraview to populate the libdir path variable
set pvlibdir [ lindex [ split "$version" "." ] 0 ].[ lindex [ split "$version" "." ] 1 ]

# Determine what compiler we're using
if { [info exists ::env(LCOMPILER) ] } {
   set compiler    $::env(LCOMPILER)
   set compver  $::env(LCOMPILERVER)
} else {
# If no compiler module loaded, use the one built against the latest provided GCC
# NOTE: The following is specific for out TOSS based systems.  May need different 
# logic for the Cray platforms.
   set compiler    gcc
   set compver 5.3.0 
   if { [module-info mode load] } {
      puts stderr "\n\t*** No compiler module loaded.  ***"
      puts stderr "\t*** Using ${compiler} ${compver} build of ParaView ***"
      module load gcc/5.3.0
   }
}

# Determine which MPI flavor and version is loaded (if any at all)
if { [info exists ::env(LMPI) ] } {
   set mpi_flav $::env(LMPI)
   set mpi_fver $::env(LMPIVER)
} else {
   #-GAC: If no MPI module is loaded, force load the Open MPI 1.6.5 module.
   if { [module-info mode load] } {
      puts stderr "\n\t*** No MPI module loaded.  ***"
      puts stderr "\t*** Loading Open MPI 1.6.5 module ***\n"
      set mpi_flav openmpi
      set mpi_fver 1.6.5
      module load openmpi/1.6.5
#      break
#      exit 1
   }
}

# path to a particular build of ParaView
set prefix /usr/projects/hpcsoft/${os}/${machine}/${name}/${version}_${compiler}-${compver}_${mpi_flav}-$mpi_fver


if { ![file exists $prefix] } {
   puts stderr "\n[module-info name]: ParaView $version is not available for this combination of\n";
   puts stderr "compiler and MPI libraries.  Please load a different combination of compiler and"
   puts stderr "MPI libraries and try again.\n"
   puts stderr "Available ParaView builds are:\n"
   puts stderr "[glob -directory /usr/projects/hpcsoft/$machine/$name -type d ${version}_*]"
   puts stderr "\n[module-info name]: $prefix: No such file or directory.\n"
   break;
   exit 1;
}
# set the needed environment variables to use Paraview
prepend-path PATH               ${prefix}/bin
prepend-path LD_LIBRARY_PATH    ${prefix}/lib:${prefix}/lib/${name}-${pvlibdir}
setenv       ParaView_DIR       ${prefix}/lib/cmake/${name}-${pvlibdir}
setenv       PARAVIEWLIBPATH	${prefix}/lib/${name}-${pvlibdir}
setenv       MESA_GL_VERSION_OVERRIDE 3.2 

# Force the PYTHONHOME and PYTHONPATH to be defined by this modulefile overriding 
# whatever the user may have:
setenv PYTHONHOME /usr/projects/hpcsoft/toss2/common/anaconda/2.1.0-python-2.7 
setenv PYTHONPATH ${prefix}/lib/${name}-${pvlibdir}/site-packages/vtk:${prefix}/lib/${name}-${pvlibdir}/site-packages:${prefix}/lib/${name}-${pvlibdir}

# Setting MANPATH is trickier - not every system sets it, nor
# do a lot of users.  So we set it if it doesn't exist
catch {set cur_manpath $env(MANPATH)}
if { ![info exists cur_manpath] || ($cur_manpath == "") } {
    # put a ':' after mandir in MANPATH so that the system defaults can still
    # be found. See the man page for 'manpath'
    setenv          MANPATH     ${prefix}/doc:
} else {
    prepend-path    MANPATH     ${prefix}/doc
}

source /usr/projects/hpcsoft/utilities/lib/envmodules_footer
