################################################################################
# Initial setup
################################################################################

# Ensure the User is able to see the DOE "Notice to Users" banner.
cat /etc/motd
sleep 5
cat $(dirname ${BASH_SOURCE})/ParaView_figlet.txt

# Validate PPN settings
if [ ${NUM_PPN} -gt ${MAX_PPN} ]
then
  echo "Warning: Reducing PPN from ${NUM_PPN} to max available of ${MAX_PPN}"
  export NUM_PPN=${MAX_PPN}
elif [ $((MAX_PPN%NUM_PPN)) -ne 0 ]
then
  echo -n "Warning: Increasing PPN from ${NUM_PPN}"
  while [ $((MAX_PPN%NUM_PPN)) -ne 0 ]
  do
    export NUM_PPN=$((NUM_PPN+1))
  done
  echo " to ${NUM_PPN} to balance workload on node"
fi

################################################################################
# Setup SSH tunnel *after* job allocation
################################################################################
setup_tunnel_post() {
  # Moab seems to not yield consistent hostlist order according to J. Patchett
  #R0HOST=$(echo $(mjobctl -q hostlist ${JOBID}) | cut -f 1 -d ',')

  # Use lower level SLURM commands instead to extract the lowest numbered node
  # in the allocation
  local R0HOST=$(scontrol show hostnames $(squeue -j ${JOBID} -o %N | tail -1) | head -1)

  if ! setup_tunnel_ssh ${R0HOST} ${PV_SERVER_PORT}
  then
    return 1
  fi
  export PV_SERVER_HOST=localhost
}

################################################################################
# Use Moab for job submission
################################################################################
submit_job() {
  if [ "${ACCOUNT}" != "DEFAULT" ]
  then
    ACCOUNT_ARG="-A ${ACCOUNT}"
  fi
  if [ "${QUEUE}" != "DEFAULT" ]
  then
    QUEUE_ARG="-q ${QUEUE}"
  fi

  export DRIVER
  local BASE_DIR=$(dirname $(readlink -f ${BASH_SOURCE}))
  export NUMABIND=${BASE_DIR}/numabind.slurm
  local MSUB_OUT="$(\
    msub -N ParaView -j oe -S /bin/bash \
      ${ACCOUNT_ARG} ${QUEUE_ARG} -l walltime=${HOURS}:00:00 \
      -l nodes=${NUM_NODES} \
      -v NUM_NODES,NUM_PPN,HOURS,PV_SERVER_HOST,PV_SERVER_PORT,PV_VERSION_FULL,DRIVER,NUMABIND \
      ${BASE_DIR}/pvserver.toss.slurm)"
  if [ $? -ne 0 ]
  then
    return 1
  fi
  JOBID=$(echo "${MSUB_OUT}" | grep -o '[0-9][0-9]*' | head -1)
  echo "  Job ID: ${JOBID}"
}

################################################################################
# Use Slurm for job status checks
################################################################################
job_status() {
  job_status_slurm $1
  return $?
}

################################################################################
# Use Moab for job cancelation
################################################################################
cancel_job() {
  cancel_job_moab $1
  return $?
}

